﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using TOP2000.Models;

namespace TOP2000.Controllers
{
    [Authorize(Roles = "admin")]
    public class lookAtUserController : Controller
    {
        public ApplicationDbContext context = new ApplicationDbContext();
        // GET: Default
        [HttpGet]

        public ActionResult Index()
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            List<VmUsersList> model = new List<VmUsersList>();
            foreach (var item in context.Users)
            {
                VmUsersList user = new VmUsersList()
                {
                    Email = item.Email,
                    Id = item.Id,
                    PasswordHash = item.PasswordHash
                };

                model.Add(user);
            }

            return View(model);
        }
    }
}