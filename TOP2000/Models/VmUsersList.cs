﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TOP2000.Models
{
    public class VmUsersList
    {
        public string Email { get; set; }
        public string Id { get; set; }
        public string PasswordHash { get; set; }
    }
}