﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace TOP2000.Controllers
{
    public class HomeController : Controller
    {
        private TOP2000Entities db = new TOP2000Entities();
        public ActionResult Index(string artiest, string title)
        {

            //als er niks word ingefult dan valt ie terug op de basis qury
            var track = from e in db.Track select e;


            //deze query doet ie allen als er een artiest is ingevult
            if (!String.IsNullOrEmpty(artiest) && String.IsNullOrEmpty(title))
            {
                track = from e in db.Track where e.Artiest.Contains(artiest) select e;
            }
            //deze query doet ie alleen als er een title is ingevult
            if (!String.IsNullOrEmpty(title) && String.IsNullOrEmpty(artiest))
            {
                track = from e in db.Track where e.Titel.Contains(title) select e;
            }
            //en deze query doet is als zowel een title als artiest word ingevult
            if (!String.IsNullOrEmpty(title) && !String.IsNullOrEmpty(artiest))
            {
                track = from e in db.Track where e.Artiest.Contains(artiest) && e.Titel.Contains(title) select e;
            }


            return View(track.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        // GET: Tracks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tracks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SongId,Titel,Artiest,Year")] Track track)
        {
            if (ModelState.IsValid)
            {
                db.Track.Add(track);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(track);
        }

        // GET: Tracks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Track track = db.Track.Find(id);
            if (track == null)
            {
                return HttpNotFound();
            }
            return View(track);
        }

        // POST: Tracks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Track track = db.Track.Find(id);
            db.Track.Remove(track);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}