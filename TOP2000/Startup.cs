﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TOP2000.Startup))]
namespace TOP2000
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
